const {test, expect} = require('@jest/globals');
const {processUrls} = require('./speaches-analyzer');

test('sample file', async () => {
    const urls = ['http://localhost:3001/test.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: "Alexander Abel",
        leastWordy: "Caesare Collins"
    });
});

test('2 sample files', async () => {
    const urls = ['http://localhost:3001/test.csv', 'http://localhost:3001/test1.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: "Alexander Abel",
        leastWordy: "Caesare Collins"
    });
});

test('2 sample file 1 speaker in 2013', async () => {
    const urls = ['http://localhost:3001/test.csv', 'http://localhost:3001/test1.csv', 'http://localhost:3001/test2.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: "Bernhard Belling",
        mostSecurity: "Alexander Abel",
        leastWordy: "Caesare Collins"
    });
});

test('1 all null', async () => {
    const urls = ['http://localhost:3001/test3.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: null,
        leastWordy: null
    });
});

test('Non existent file', async () => {
    const urls = ['http://localhost:3001/notexists.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: null,
        leastWordy: null
    });
});

test('1 of multiple is Non existent file', async () => {
    const urls = ['http://localhost:3001/test1.csv', 'http://localhost:3001/notexists.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: null,
        leastWordy: null
    });
});

test('a file with wrong headers', async () => {
    const urls = ['http://localhost:3001/noheader.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: null,
        leastWordy: null
    });
});

test('a file with wrong number and date formats', async () => {
    const urls = ['http://localhost:3001/wrongformats.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: 'Alexander Abel',
        leastWordy: null
    });
});

test('custom file - all petr Novak', async () => {
    const urls = ['http://localhost:3001/test4.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: 'Petr Novak',
        mostSecurity: 'Petr Novak',
        leastWordy: 'Petr Novak'
    });
});

test('Just one record file- all Petr Novak', async () => {
    const urls = ['http://localhost:3001/test5.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: 'Petr Novak',
        mostSecurity: 'Petr Novak',
        leastWordy: 'Petr Novak'
    });
});

test('Empty file', async () => {
    const urls = ['http://localhost:3001/test6.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: null,
        mostSecurity: null,
        leastWordy: null
    });
});

test('Empty file + 1 record', async () => {
    const urls = ['http://localhost:3001/test6.csv', 'http://localhost:3001/test5.csv'];
    const result = await processUrls(urls);

    expect(result).toStrictEqual({
        mostSpeeches: 'Petr Novak',
        mostSecurity: 'Petr Novak',
        leastWordy: 'Petr Novak'
    });
});
