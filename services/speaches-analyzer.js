const axios = require('axios');
const csvParser = require('csv-parser');
const {Readable} = require('stream');
const dateParser = require('date-format-parse');

const processUrls = async (urls) => {
    let loadedData = await loadAndParseCVS(urls);
    let stats = getRawStats(loadedData);
    return getAnswers(stats);
}


const loadAndParseCVS = async (urls) => {
    let data = [];
    let loaders = [];

    try {
        for (let url of urls) {
            let loader = new Promise(async (resolve, reject) => {
                const results = [];
                try {
                    const response = await axios.get(url);
                    Readable.from(response.data)
                        .pipe(csvParser({
                            mapValues: ({header, index, value}) => value.trim(),
                            mapHeaders: ({header, index}) => header.trim()
                        }))
                        .on('headers', (headers) => {
                            if (headers[0] !== 'Speaker' || headers[1] !== 'Topic' || headers[2] !== 'Date' || headers[3] !== 'Words')
                                reject(`Wrong headers in file ${url}`);
                        })
                        .on('data', (data) => {
                            results.push(data);
                        })
                        .on('end', () => resolve(results));
                } catch (e) {
                    console.error(e);
                    reject(e);
                }
            });
            loaders.push(loader);
        }
        //process all urls and merge the result into a single array
        [...await Promise.all(loaders)].forEach(partial => data = [...data, ...partial]);
    } catch (e) {
        console.error(e);
    }
    return data;
}

const getRawStats = (data) => {
    const stats = [];
    //calculate statistics
    data.forEach(record => {
        const {Speaker, Words, Topic, Date} = record;
        let curStat = stats.find(el => el.speaker === Speaker);
        //let curStat = stats.get(Speaker);
        if (curStat) {
            curStat.speachCount += (dateParser.parse(Date, 'YYYY-MM-DD').getFullYear() === 2013) ? 1 : 0;
            curStat.wordsCount += parseInt(Words);
            curStat.secSpeachCount += (Topic === 'Innere Sicherheit' || Topic === 'Inner Security') ? 1 : 0;
        } else {
            curStat = {
                speaker: Speaker,
                speachCount: (dateParser.parse(Date, 'YYYY-MM-DD').getFullYear() === 2013) ? 1 : 0,
                wordsCount: parseInt(Words),
                secSpeachCount: (Topic === 'Innere Sicherheit' || Topic === 'Inner Security') ? 1 : 0
            }
            stats.push(curStat);
        }
        //stats.set(Speaker, curStat);
    });

    return stats;
}


const getAnswers = (stats) => {

    let mostSpeachesCount = 0;
    let mostSpeachesName = null;
    let maxSpeachCounter = 0;
    let mostSecurityCount = 0;
    let mostSecurityName = null;
    let maxSecurityCounter = false;
    let leastWordyCount = -1;
    let leastWordyName = null;
    let leastWordyCounter = false;

    stats.forEach(({speaker, speachCount, wordsCount, secSpeachCount}) => {
        //most frequent speaker
        if (speachCount > mostSpeachesCount) {
            mostSpeachesCount = speachCount;
            mostSpeachesName = speaker;
            maxSpeachCounter = 1;
        } else if (speachCount === mostSpeachesCount && speachCount > 0) {
            //we have the same max speach count, increase the counter to detect non unique answers
            maxSpeachCounter++;
        }
        //most security
        if (secSpeachCount > mostSecurityCount) {
            mostSecurityCount = secSpeachCount;
            mostSecurityName = speaker;
            maxSecurityCounter = 1;
        } else if (secSpeachCount === mostSecurityCount && secSpeachCount > 0) {
            //we have the same max security speach count, increase the counter to detect non unique answers
            maxSecurityCounter++;
        }
        //least wordy
        if (!isNaN(wordsCount) && (wordsCount < leastWordyCount || leastWordyCount === -1)) {
            leastWordyCount = wordsCount;
            leastWordyName = speaker;
            leastWordyCounter = 1;
        } else if (wordsCount === leastWordyCount && leastWordyCount > 0) {
            //we have the same least wordy count, increase the counter to detect non unique answers
            leastWordyCounter++;
        }
    });

    // dela with non unique answers
    if (maxSpeachCounter > 1) {
        mostSpeachesName = null;
    }

    if (maxSecurityCounter > 1) {
        mostSecurityName = null;
    }

    if (leastWordyCounter > 1) {
        leastWordyName = null;
    }


    const answers = {mostSpeeches: mostSpeachesName, mostSecurity: mostSecurityName, leastWordy: leastWordyName};
    console.log(stats);
    return answers;
}

module.exports.processUrls = processUrls;