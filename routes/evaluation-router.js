var express = require('express');
const {processUrls} = require('../services/speaches-analyzer');

var router = express.Router();


router.get('/', async function (req, res, next) {
    const {url} = req.query;
    if (!url || url.length === 0) {
        res.json({mostSpeeches: null, mostSecurity: null, leastWordy: null});
        return;
    }

    //convert url query parameters into an array so we can process uniformly
    const urls = typeof url === 'string' ? [url] : [...url];

    res.json(await processUrls(urls));

});


module.exports = router;
